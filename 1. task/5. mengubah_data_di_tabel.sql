-- Sebagai contoh, ubah stok produk dengan nama_produk 'Kartu Remi' -> menjadi '0', dengan query berikut:
UPDATE produk SET stok = 0 WHERE nama_produk = 'Kartu Remi';