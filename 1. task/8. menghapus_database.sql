-- Sebagai contoh, hapus database retail_online, dengan query berikut:
DROP DATABASE IF EXISTS `retail_online`;
-- Tapi jika menghapus database, maka akan menghapus semua yang ada di dalam, termasuk data dan tabel juga :)