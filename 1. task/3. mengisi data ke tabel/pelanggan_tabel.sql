INSERT INTO
    `pelanggan` (
        `id`,
        `nama`,
        `email`,
        `alamat`,
        `no_hp`
    )
VALUES (
        1,
        'Pratama Jaeman',
        'pratama.jaeman@gmail.com',
        'Dk. Padang No. 906, Depok 17172, JaTim',
        '08128803992'
    ), (
        2,
        'Luthfi Agustina Karin',
        'luthfi.agustina@gmail.co.id',
        'Gg. Gedebage Selatan No. 209, Pagar Alam 23988, SulBar',
        '08142868809 '
    ), (
        3,
        'Carla Nuraini',
        'carla1920@gmail.com',
        'Ki. Baik No. 958, Bima 57750, SumUt',
        '08124758120'
    ), (
        4,
        'Laksana Damanik',
        'laksannik@gmail.com',
        'Jln. Sugiono No. 200, Banda Aceh 34791, KalSel',
        '08296849788'
    ), (
        5,
        'Carub Haryanto',
        'carubnto@yahoo.com',
        'Dk. Yos Sudarso No. 476, Bima 40230, Maluku',
        '08129551351'
    ), (
        6,
        'Najwa Melani',
        'najwani@latupono.com',
        'Gg. Rumah Sakit No. 209, Tual 38829, Jambi',
        '08128459793'
    ), (
        7,
        'Eluh Saragih',
        'eluhgih4j9@purnawati.mail.com',
        'Gg. W.R. Supratman No. 604, Prabumulih 48096, Lampung',
        '08128082358'
    ), (
        8,
        'Adiarja Putra',
        'adiartra23@gmail.com',
        'Kpg. Jambu No. 217, Tebing Tinggi 22030, KalTim',
        '08146957202'
    ), (
        9,
        'Wecitra',
        'wcitra@mail.com',
        'Gg. Remaja No. 209, Pagar Alam 23988, SulBar',
        '08950837910'
    ), (
        10,
        'Calum Agung',
        'c.agung@mail.com',
        'Jln. Salak No. 200, Banda Aceh 34791, KalSel',
        '08599281801'
    ), (
        11,
        'Andara Luthfi',
        'andrluth234@mail.com',
        'Jr. Bakau Griya Utama No. 395, Banjarmasin 28785, PapBar',
        '08128393812'
    );
