INSERT INTO
    `produk` (
        `id`,
        `kode_produk`,
        `nama_produk`,
        `harga_produk`,
        `stok`,
        `kategori_id`,
        `tanggal_ditambahkan`
    )
VALUES (
        1,
        'P001',
        'Chitato Sapi Panggang 68GR',
        12000,
        100,
        2,
        CURRENT_TIMESTAMP
    ), (
        2,
        'P002',
        'Piattos Snack Kentang Rumput Laut 50GR',
        10000,
        80,
        2,
        CURRENT_TIMESTAMP
    ), (
        3,
        'P003',
        'Sunlight Pencuci Piring Jeruk Nipis 650ML',
        15000,
        80,
        10,
        CURRENT_TIMESTAMP
    ), (
        4,
        'P004',
        'Rinso Detergen Liquid Matic Top Load 700ML',
        25500,
        60,
        10,
        CURRENT_TIMESTAMP
    ), (
        5,
        'P005',
        'Japota Potato Chips Sambal Bawang 68GR',
        8000,
        80,
        2,
        CURRENT_TIMESTAMP
    ), (
        6,
        'P006',
        'Nu Minuman Choco Tea Hazeltea 330ML',
        8500,
        90,
        1,
        CURRENT_TIMESTAMP
    ), (
        7,
        'P007',
        'Cutter Sistem Putar',
        14200,
        90,
        8,
        CURRENT_TIMESTAMP
    ), (
        8,
        'P008',
        'Snowman Spidol Giant Hitam',
        13100,
        100,
        8,
        CURRENT_TIMESTAMP
    ), (
        9,
        'P009',
        'Abc Coffee Drink Milk Coffee 200ML',
        4000,
        40,
        1,
        CURRENT_TIMESTAMP
    ), (
        10,
        'P010',
        'So Klin Detergent Cair Violet Blossom 720ML',
        15500,
        15,
        10,
        CURRENT_TIMESTAMP
    ), (
        11,
        'P011',
        'Lifebuoy Body Wash Lemon Fresh 100ML',
        16700,
        20,
        3,
        CURRENT_TIMESTAMP
    ), (
        12,
        'P012',
        'Rejoice Shampoo Anti Ketombe 3In1 340ML',
        54000,
        50,
        3,
        CURRENT_TIMESTAMP
    ), (
        13,
        'P013',
        'Kartu Remi',
        12000,
        12,
        4,
        CURRENT_TIMESTAMP
    ), (
        14,
        'P014',
        'Maybelline Lash Sensational Mascara Sky High 6ML',
        140000,
        14,
        5,
        CURRENT_TIMESTAMP
    ), (
        15,
        'P015',
        'Uno Express Permainan Uno',
        25500,
        5,
        4,
        CURRENT_TIMESTAMP
    ), (
        16,
        'P016',
        'Mama Lemon Cairan Pencuci Piring Extra Clean Jeruk Nipis 750ML',
        29500,
        30,
        10,
        CURRENT_TIMESTAMP
    ), (
        17,
        'P017',
        'Facial Cotton 50GR',
        10100,
        100,
        5,
        CURRENT_TIMESTAMP
    ), (
        18,
        'P018',
        'Sosro Fruit Tea 350ML',
        3500,
        30,
        1,
        CURRENT_TIMESTAMP
    ), (
        19,
        'P019',
        'T-Shirt Damn I Love Indonesia White (Limited)',
        12500,
        12,
        6,
        CURRENT_TIMESTAMP
    ), (
        20,
        'P020',
        'Sandal Wanita Uk 37-38 Psg',
        51000,
        5,
        6,
        CURRENT_TIMESTAMP
    ), (
        21,
        'P021',
        'Tolak Angin 5 Sachet',
        15000,
        100,
        7,
        CURRENT_TIMESTAMP
    ), (
        22,
        'P022',
        'Bola Kasti',
        80000,
        10,
        9,
        CURRENT_TIMESTAMP
    ), (
        23,
        'P023',
        'Obat Batuk FIX',
        29900,
        100,
        7,
        CURRENT_TIMESTAMP
    ), (
        24,
        'P024',
        'Kopi Kenangan Alpukat 100ML',
        11200,
        40,
        1,
        CURRENT_TIMESTAMP
    ), (
        25,
        'P025',
        'Qtela Keripik Singkong Ayam Lada Hitam 100GR',
        9700,
        70,
        2,
        CURRENT_TIMESTAMP
    );