INSERT INTO
    `transaksi` (
        `id`,
        `kode_transaksi`,
        `quantity`,
        `produk_id`,
        `pelanggan_id`,
        `tanggal_transaksi`
    )
VALUES (NULL, 'T001', 2, 1, 1, NOW()), (NULL, 'T002', 1, 24, 9, NOW()), (NULL, 'T003', 1, 20, 9, NOW()), (NULL, 'T004', 1, 1, 9, NOW()), (NULL, 'T005', 1, 25, 8, NOW()), (NULL, 'T006', 2, 16, 8, NOW()), (NULL, 'T007', 2, 5, 10, NOW()), (NULL, 'T008', 2, 9, 10, NOW()), (NULL, 'T009', 1, 1, 10, NOW()), (NULL, 'T010', 1, 1, 1, NOW());