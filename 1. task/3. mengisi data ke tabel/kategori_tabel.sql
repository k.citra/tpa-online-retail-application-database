INSERT INTO
    `kategori` (
        `id`,
        `nama_kategori`,
        `deskripsi`,
        `slug`
    )
VALUES (
        1,
        'Minuman Ringan',
        'Kategori Produk: Minuman Ringan',
        'minuman-ringan'
    ), (
        2,
        'Makanan Ringan',
        'Kategori Produk: Makanan Ringan',
        'makanan-ringan'
    ), (
        3,
        'Kebersihan',
        'Kategori Produk: Kebersihan Dan Perawatan',
        'kebersihan'
    ), (
        4,
        'Mainan',
        'Kategori Produk: Mainan Anak',
        'mainan'
    ), (
        5,
        'Kosmetik',
        'Kategori Produk: Kosmetik',
        'kosmetik'
    ), (
        6,
        'Fashion',
        'Kategori Produk: Fashion',
        'fashion'
    ), (
        7,
        'Kesehatan',
        'Kategori Produk: Obat Dan Kesehatan',
        'kesehatan'
    ), (
        8,
        'Alat Tulis',
        'Kategori Produk: Alat Tulis Dan Kantor',
        'alat-tulis'
    ), (
        9,
        'Olahraga',
        'Kategori Produk: Olahraga',
        'olahraga'
    ), (
        10,
        'Rumah Tangga',
        'Kategori Produk: Rumah Tangga',
        'rumah-tangga'
    );